
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import * as React from 'react';
import { ThemeProvider } from 'react-native-elements'

import store from './src/store/Store';
import { Provider } from 'react-redux';


export default function Main() {
    return (

        <ThemeProvider>
            <Provider store={store}>
                <App />
            </Provider>
        </ThemeProvider>
    );
}

AppRegistry.registerComponent(appName, () => Main);

