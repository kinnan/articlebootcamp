import React, { memo, useEffect, useState } from 'react';
import Background from '../components/Background';
import { useDispatch, useSelector } from 'react-redux';
import { TouchableOpacity } from 'react-native';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Paragraph from '../components/Paragraph';
import Button from '../components/Button';
import { useIsFocused } from '@react-navigation/native';
import { logout } from '../store/actions/auth';

const Dashboard = ({ navigation }) => {
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const { isLoggedIn } = useSelector((state) => state.auth);



  useEffect(() => {
    console.log("logged in ", isLoggedIn);
    if (isLoggedIn === false) {
      navigation.navigate('LoginScreen');
    }
  }, [isLoggedIn, isFocused]);

  return (
    <Background>
      <TouchableOpacity
      onPress={() => navigation.navigate("Profile")}
    >
      <Logo />
    </TouchableOpacity>
      <Header>Kompas dot com</Header>
      <Paragraph>
        We are a very out-of-date news portal, and our authors are incompetent and some of them are crazy.
    </Paragraph>
      <Button mode="outlined" onPress={() => {
        dispatch(logout())
      }}>
        Logout
    </ Button>
    </Background>
  );
}

export default memo(Dashboard);
