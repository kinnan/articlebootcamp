
import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { Card } from 'react-native-elements';
import Button from '../components/Button';
import Header from '../components/Header';
import TextInput from '../components/TextInput';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { ScrollView } from 'react-native-gesture-handler';


const EditArticle = ({ route, navigation }) => {
    const { idArticle, titleArticle, contentArticle } = route.params;
    const [content, setContent] = useState('');
    const [title, setTitle] = useState('');
    const { user } = useSelector((state) => state.auth)

    const updateArticle = async (e) => {
        const header = {
            'Content-Type': 'application/json',
            'Authorization': user.accessToken,
        }
        axios
            .put(`https://user-article.herokuapp.com/api/articles/${idArticle}`,
                {
                    title: title,
                    content: content,
                },
                {
                    headers: header
                }
            )
            .then(function (res) {

                console.log('res ' + res.data);
                navigation.goBack();
                alert('Article up to date');

            })
            .catch(function (error) {
                console.log(JSON.stringify(error.response, null, 2));
                alert(JSON.stringify(error.response, null, 2));
            });

    }

    const DeleteArticle = async (e) => {
        const header = {
            'Content-Type': 'application/json',
            'Authorization': user.accessToken,
        }
        axios
            .delete(`https://user-article.herokuapp.com/api/articles/${idArticle}`,
                {
                    headers: header
                }
            )
            .then(function (res) {
                alert('Article deleted')
                navigation.goBack();
                setIsLoading(true)
            })
            .catch(function (error) {
                console.log(JSON.stringify(error.response, null, 2));
                // alert(JSON.stringify(error.response, null, 2));
            });

    }

    return (
        <>
            <ScrollView>
                <View style={{ justifyContent: 'center' }}>
                    <Card >
                        <Header>Edit Article</Header>
                        <Card.Divider />
                        <TextInput
                            defaultValue={titleArticle}
                            onChangeText={title => setTitle(title)}
                        />
                        <ScrollView
                            keyboardShouldPersistTaps="always">
                            <TextInput
                                defaultValue={contentArticle}
                                onChangeText={content => setContent(content)}
                                multiline={true}
                                numberOfLines={15}

                            />
                        </ScrollView>

                        <Button mode="outlined" onPress={() => updateArticle()} >
                            Submit
                    </Button>
                        <Button mode="contained" onPress={() => DeleteArticle()}>
                            Delete
                    </Button>
                    </Card>
                </View>
            </ScrollView>
        </>
    );
};


export default EditArticle;
