import React, { useState, useEffect } from 'react';
import { Text, Divider, Card, Input } from 'react-native-elements';
import Header from '../components/Header';
import { View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useIsFocused } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import axios from 'axios';
import { ScrollView } from 'react-native-gesture-handler';

export default function ArticleDetail({ route, navigation }) {
    const { idArticle } = route.params;
    console.log("Article id " + idArticle);
    const isFocused = useIsFocused();
    const [loading, setLoading] = useState(false);
    const [comment, setComment] = useState('')
    const [comments, setComments] = useState([{}])
    const [data, setData] = useState({ user: { id: 0, name: "" }, content: "", createdAt: "", })
    const { user } = useSelector((state) => state.auth)

    const postToken = async (e) => {
        axios
            .get(`https://user-article.herokuapp.com/api/articles/${idArticle}`)
            .then(function (res) {
                // console.log(res.data.article);
                setData(res.data.article);
            })
            .catch(function (error) {
                // console.log(JSON.stringify(error));
            })
    }

    const getComments = () => {
        const header = {
            'Content-Type': 'application/json',
            'Authorization': user.accessToken,
        }
        axios
            .get(`https://user-article.herokuapp.com/api/articles/${idArticle}/comments`,
                {
                    headers: header,
                }
            )
            .then(function (res) {
                console.log("Comments " + JSON.stringify(res.data.comments));

                setComments(res.data.comments)
            })
            .catch(function (error) {
                console.log(JSON.stringify(error));
            })
    }


    const postComment = () => {
        const header = {
            'Content-Type': 'application/json',
            'Authorization': user.accessToken,
        }
        axios
            .post(`https://user-article.herokuapp.com/api/users/comments/${idArticle}`,
                {
                    content: comment,
                },
                {
                    headers: header
                }
            )
            .then(function (res) {
                console.log(JSON.stringify(res));
                alert('Comment posted')
                setLoading(!loading)

            })
            .catch(function (error) {
                console.log(JSON.stringify(error.response, null, 2));
                alert(JSON.stringify(error.response, null, 2));
            });

    }

    useEffect(() => {
        postToken()
        getComments()
    }, [isFocused, loading]);


    return (
        <>
            <ScrollView >
                <Header key={data.id}>{data.title}</Header>
                {data.user === null ? (
                <Text style={styles.Text} h5>Uploader: Anonim </Text>) : (
                <Text style={styles.Text} h5>Uploader: {data.user.name} </Text>)}

                <Text style={styles.Text} h5>created at: {data.createdAt.substring(0, 10)}</ Text>
                <Text style={{ textAlign: 'center', marginBottom: 15 }} h5>updated at: {data.updateAt}</ Text>
                < Divider />
                <Text style={{ padding: 10 }}>{data.content}</Text>
                <Divider style={{ marginBottom: 10 }} />

                {comments.length === 0 ? (
                    <Text>No comments </Text>) : (
                        comments.map((comments) => {
                            return (
                                <Card>
                                    <Text key={comments.id}>{comments.content}</Text>
                                </Card>
                            )
                        })
                    )
                }

                <View>
                    <Input
                        placeholder='Write a comment...'
                        rightIcon={
                            <Icon
                                name="send-o"
                                onPress={() => postComment()}
                                size={20}
                                color="#0A5388"
                            />
                        }
                        onChangeText={comment => setComment(comment)}
                    />
                </View>
            </ScrollView>
        </>
    );
}

const styles = StyleSheet.create({
    Text: {
        textAlign: 'center'
    },
})