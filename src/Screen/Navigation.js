import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/FontAwesome';
import Articles from './Articles';
import ArticleDetails from './ArticleDetails';
import LoginScreen from './LoginScreen';
import RegisterScreen from './RegisterScreen';
import UserAccount from './UserAccount';
import MyArticle from './MyArticle';
import CreateArticle from './CreateArticle';
import EditArticle from './EditArticle';
import Profile from './Profile';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createMaterialBottomTabNavigator();

function MyDrawer() {
    return (
        <Drawer.Navigator>
            <Drawer.Screen name="Account" component={UserAccount} />
            <Drawer.Screen name="My Article" component={MyArticle} />
            <Drawer.Screen name="Profile" component={Profile} />
        </Drawer.Navigator>
    )
}

function MyTabs() {
    return (
        <Tab.Navigator
            initialRouteName="Home"
            tabBarOptions={{
                activeTintColor: '#FFFFFF',
            }}
            barStyle={{ backgroundColor: '#0A5388' }}
        >
            <Tab.Screen
                name="Home"
                component={Articles}
                options={{
                    tabBarIcon: ({ color }) => (
                        <Icon name="th-large"
                            color={color}
                            size={23} />
                    )
                }} />

            <Tab.Screen name="Account"
                component={MyDrawer}
                options={{
                    tabBarIcon: ({ color }) =>
                        <Icon name="wheelchair-alt"
                            color={color}
                            size={23} />
                }} />



        </Tab.Navigator>
    )
}


function Navigator() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName='MyTabs' screenOptions={{ headerShown: false }}>
                <Stack.Screen name="MyTabs" component={MyTabs} />
                <Stack.Screen name="ArticleDetails" component={ArticleDetails} />
                <Stack.Screen name="LoginScreen" component={LoginScreen} />
                <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
                <Stack.Screen name="UserAccount" component={UserAccount} />
                <Stack.Screen name="Profile" component={Profile} />
                <Stack.Screen name="MyArticle" component={MyArticle} />
                <Stack.Screen name="CreateArticle" component={CreateArticle} />
                <Stack.Screen name="Articles" component={Articles} />
                <Stack.Screen name="EditArticle" component={EditArticle} />

            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default Navigator;
