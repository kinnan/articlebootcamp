import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import TextInput from '../components/TextInput';
import { theme } from '../core/theme';
import Button from '../components/Button';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';

const Profile = ({ route, navigation }) => {
    const isFocused = useIsFocused();
    const [isLoading, setLoading] = useState(true);
    const [name, setName] = useState('');
    const [username, setuserName] = useState('');
    const [email, setEmail] = useState('');
    const { user } = useSelector((state) => state.auth);
    const [data, setData] = useState([{}]);
    const [filePath, setFilePath] = useState({});

    const getProfile = async (e) => {
        const header = {
            'Content-Type': 'application/json',
            'Authorization': user.accessToken,
        }
        axios
            .get(`https://user-article.herokuapp.com/api/users`,
                {
                    headers: header
                }
            )
            .then(function (res) {
                setData(res.data.user)
                console.log('res ' + res.data.user);

            })
            .catch(function (error) {
                console.log(JSON.stringify(error.response, null, 2));
            });
    }
    
    const updateProfile = async (e) => {
        const header = {
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + user.accessToken,
        }
        axios
            .put(`https://user-article.herokuapp.com/api/users/`,
                {
                    name: name,
                    username: username,
                    email: email,
                },
                {
                    headers: header
                }
            )
            .then(function (res) {
                navigation.goBack();
                alert('Profile up to date');

            })
            .catch(function (error) {
                console.log(JSON.stringify(error.response, null, 2));
                alert(JSON.stringify(error.response, null, 2));
            });
    }

    useEffect(() => {
        getProfile();
    }, [isFocused, isLoading]);

    return (
        <>
            <View style={styles.topBar}></View>
            <View style={styles.container}>
                <Image style={styles.avatar} source={{ uri: 'https://bootdey.com/img/Content/avatar/avatar6.png' }} />
                <TextInput
                    label="Name"
                    defaultValue={data.name}
                    onChangeText={name => setName(name)}>
                </TextInput>
                <TextInput
                    label="Username"
                    autoCapitalize="none"
                    defaultValue={data.username}
                    onChangeText={username => setuserName(username)}>
                </TextInput>
                <TextInput
                    label="Email"
                    autoCapitalize="none"
                    keyboardType="email-address"
                    defaultValue={data.email}
                    onChangeText={email => setEmail(email)}>
                </TextInput>
                {/* <TextInput
                        label="Password" 
                        defaultValue={user.password}
                        onChangeText={password => setPassword(password)}>
                    </TextInput> */}
                <Button mode="contained" onPress={() => updateProfile()} >
                    Submit
                    </ Button>
            </View>
        </>
    );
}

const styles = StyleSheet.create({
    topBar: {
        backgroundColor: "#1299F6",
        height: 200,
    },
    avatar: {
        // flex: 1,
        width: 150,
        height: 150,
        borderRadius: 100,
        borderWidth: 4,
        borderColor: "white",
        marginBottom: 10,
        // alignSelf: 'center',
        // // position: 'absolute',
        marginTop: -280
    },
    container: {
        flex: 1,
        padding: 20,
        width: '100%',
        maxWidth: 340,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    input: {
        backgroundColor: theme.colors.surface,
    },
    error: {
        fontSize: 14,
        color: theme.colors.error,
        paddingHorizontal: 4,
        paddingTop: 4,
    },
});

export default Profile;