import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { Card, Text } from 'react-native-elements';
import Button from '../components/Button';
import Header from '../components/Header';
import TextInput from '../components/TextInput';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { ScrollView } from 'react-native-gesture-handler';

const CreateArticle = ({ navigation }) => {
  const [content, setContent] = useState('');
  const [title, setTitle] = useState('');
  const { user } = useSelector((state) => state.auth)

  const makeArticle = async (e) => {
    const header = {
      'Content-Type': 'application/json',
      'Authorization': user.accessToken,
    }
    axios
      .post('https://user-article.herokuapp.com/api/articles', {
        title: title,
        content: content,
      },
        {
          headers: header,
        }
      )
      .then(function (res) {
        console.log('res' + res.data);
        alert("Create article succesfully")
        navigation.navigate('Home');
      })
      .catch(function (error) {
        console.log(JSON.stringify(error.response, null, 2));
        alert(JSON.stringify(error.response, null, 2));
      });
  }

  return (
    <>
      <ScrollView>
        <View style={{ justifyContent: 'center' }}>
          <Card>
            <Header>Kompas dot com</Header>
            <Card.Divider />
            <TextInput
              label="Title"
              onChangeText={title => setTitle(title)}
            />
            <ScrollView
              keyboardShouldPersistTaps="always">
              <TextInput
                label="Title"
                onChangeText={content => setContent(content)}
                multiline={true}
                numberOfLines={15}
              />
            </ScrollView>
            <Button
              mode="contained" onPress={() => makeArticle()} >Submit
          </Button>
          </Card>
        </View>
      </ScrollView>
    </>
  );
};

export default CreateArticle;