import React, { useEffect, useState } from "react";
import { ScrollView, TouchableOpacity, StyleSheet, View } from "react-native";
import { Text, Card } from "react-native-elements";
import Header from "../components/Header";
import axios from "axios";
import { useIsFocused } from '@react-navigation/native';

const Home = ({ navigation }) => {
  const isFocused = useIsFocused();
  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(true);

  const postArticle = async () => {
    await axios
      .get("https://user-article.herokuapp.com/api/articles")
      .then((res) => {
        // console.log(res);
        setData(res.data.article);
      })
      .catch((error) => {
        // console.log(error.res.data, null, 2);
      });
  };

  useEffect(() => {
    postArticle();
  }, [isFocused, isLoading]);

  return (
    <View style={styles.container}>
      <ScrollView>
        {data.length === 0 ? (
          <Header> No data </Header>
        ) : (
            data.map((Article) => {
              return (
                <>
                  <Card style={{ paddingBottom: 30 }}>
                    <TouchableOpacity
                      onPress={() => navigation.navigate("ArticleDetails",
                        { idArticle: Article.id })}
                    >
                      <Card.Title h4>{Article.title}</Card.Title>

                      {Article.user === null ? (
                        <Card.Title h5>Uploader: Anonim </Card.Title>) : (
                          <Card.Title h5>Uploader: {Article.user.name} </Card.Title>)}

                      <Card.Divider style={{ marginBottom: 50 }}>
                        <Text style={styles.cardtext} numberOfLines={3}>
                          {Article.content}
                        </Text>
                      </Card.Divider>
                    </TouchableOpacity>
                  </Card>
                </>
              );
            })
          )}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  theme: {
    flex: 1,
    alignItems: "center",
    paddingTop: 10,
  },
  cardtext: {
    paddingTop: 5,
    marginBottom: 10,
  },
});

export default Home;