import React, { memo, useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused } from '@react-navigation/native';
import Background from '../components/Background';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import BackButton from '../components/BackButton';
import { theme } from '../core/theme';
import { login } from '../store/actions/auth';

const LoginScreen = ({ navigation }) => {
  const isFocused = useIsFocused();
  const [isLoading, setLoading] = useState(true);
  const [username, setuserName] = useState('');
  const [password, setPassword] = useState('');
  const { isLoggedIn } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const _onLoginPressed = (e) => {
    dispatch(login(username, password))
      .then(() => {
        navigation.navigate('Account');
      })
      .catch(() => {
        alert('User is incorrect')
      });
  };

  useEffect(() => {
    console.log("Already login? ", isLoggedIn);
    if (isLoggedIn === true) {
      navigation.navigate('Account');
    }
  }, [isLoggedIn, isFocused]);

  return (
    <Background>
      <BackButton goBack={() => navigation.navigate('Home')} />
      <Logo />
      <Header>Kompas dot com</Header>
      <TextInput
        label="Username"
        returnKeyType="next"
        autoCapitalize="none"
        value={username.value}
        onChangeText={username => setuserName(username)}
        error={!!username.error}
        errorText={username.error}
      />
      <TextInput
        label="Password"
        returnKeyType="done"
        autoCapitalize="none"
        value={password.value}
        onChangeText={password => setPassword(password)}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />
      <Button mode="contained" onPress={_onLoginPressed}>
        Login
      </Button>
      <View style={styles.row}>
        <Text style={styles.label}>Don’t have an account? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('RegisterScreen')}>
          <Text style={styles.link}>Sign up</Text>
        </TouchableOpacity>
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  label: {
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
});

export default memo(LoginScreen);
