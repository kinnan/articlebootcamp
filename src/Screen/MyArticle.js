import React, { useEffect, useState } from "react";
import { ScrollView, TouchableOpacity, StyleSheet, View } from "react-native";
import { Text, Card } from "react-native-elements";
import axios from "axios";
import { useSelector } from 'react-redux';
import { useIsFocused } from '@react-navigation/native';
import Button from "../components/Button";

const MyArticle = ({ navigation }) => {
    const [data, setData] = useState([{
        id: 0, title: "", content: "", user: [{ id: 0, name: "" }]
    }]);
    const isFocused = useIsFocused();
    const { user } = useSelector((state) => state.auth)
    const [isLoading, setLoading] = useState(true);

    const header = {
        "Content-Type": "application/json",
        Authorization: user.accessToken,
    };

    const postArticle = async () => {
        await axios
            .get(`https://user-article.herokuapp.com/api/users/${user.id}/articles`, {
                headers: header,
            })
            .then((res) => {
                console.log(res);
                setData(res.data.article);
            })
            .catch((error) => {
                console.log(error);
            });
    };

    useEffect(() => {
        postArticle();
    }, [isFocused, isLoading]);


    return (
        <View style={styles.container}>
            <ScrollView>
                {data.length === 0 ? (
                    <Text> No data </Text>
                ) : (
                        data.map((Article) => {
                            return (
                                <>
                                    <Card style={{ paddingBottom: 30 }}>
                                        <View style={{ flexDirection: 'row' }}>
                                        </View>
                                        <TouchableOpacity onPress={() => navigation.navigate("EditArticle",
                                            {
                                                idArticle: Article.id,
                                                titleArticle: Article.title,
                                                contentArticle: Article.content
                                            })}>
                                            <Card.Title h4>{Article.title}</Card.Title>
                                            <Card.Divider style={{ marginBottom: 50 }}>
                                                <Text style={styles.cardtext} numberOfLines={3}>
                                                    {Article.content}
                                                </Text>
                                            </Card.Divider>
                                        </TouchableOpacity>
                                    </Card>
                                </>
                            );
                        })
                    )}
            </ScrollView>
            <Button style={styles.button}
                mode="outlined"
                onPress={() => navigation.navigate('CreateArticle')}>
                Create article
    </Button>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
    theme: {
        flex: 1,
        alignItems: "center",
        paddingTop: 10,
    },
    cardtext: {
        paddingTop: 5,
        marginBottom: 10,
    },
    button: {
        marginTop: 20,
        width: '80%',
        padding: 5,
        borderRadius: 10,
        alignSelf: 'center'
    },
});

export default MyArticle;