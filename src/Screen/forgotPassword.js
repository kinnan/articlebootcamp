import React, { memo, useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused } from '@react-navigation/native';
import Background from '../components/Background';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import BackButton from '../components/BackButton';
import { theme } from '../core/theme';
import { login } from '../store/actions/auth';
import axios from 'axios'

const LoginScreen = ({ navigation }) => {
  const isFocused = useIsFocused();
  const [isLoading, setLoading] = useState(true);
  const [username, setuserName] = useState('');
  const [password, setPassword] = useState('');
  const { isLoggedIn } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const _onLoginPressed = (e) => {
    dispatch(login(username, password))
      .then(() => {
        navigation.navigate('Account');
      })
      .catch(() => {
        alert('User is incorrect')
      });
  };

  const header = {
    "Content-Type": "text/html"
  };

  const forgotPassword = () => {
    return axios
      .get('https://user-article.herokuapp.com/api/users/password', {
        headers: header,
      })

      .then(response => {
        console.log(JSON.stringify(response))
      });
  };

  useEffect(() => {
    console.log("Already login? ", isLoggedIn);
    if (isLoggedIn === true) {
      navigation.navigate('Account');
    }
  }, [isLoggedIn, isFocused]);

  return (
    <Background>
      <BackButton goBack={() => navigation.navigate('LoginScreen')} />

      <Logo />

      <Header>Restore Password</Header>

      <TextInput
        label="E-mail address"
        returnKeyType="done"
        value={email.value}
        onChangeText={text => setEmail({ value: text, error: '' })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />

      <Button mode="contained" onPress={_onSendPressed} style={styles.button}>
        Send Reset Instructions
      </Button>

      <TouchableOpacity
        style={styles.back}
        onPress={() => navigation.navigate('LoginScreen')}
      >
        <Text style={styles.label}>← Back to login</Text>
      </TouchableOpacity>
    </Background>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  label: {
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
});

export default memo(LoginScreen);
