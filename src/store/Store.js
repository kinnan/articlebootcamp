import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import AsyncStorage from '@react-native-community/async-storage';
import { setInit } from './actions/auth';

const middleware = [thunk];
const store = createStore(rootReducer, applyMiddleware(...middleware));

const getAsyncStorage = () => {
    return (dispatch) => {
        AsyncStorage.getItem('user').then((result) => {
            dispatch(setInit(result));
        });
    };
};

store.dispatch(getAsyncStorage());

export default store;
