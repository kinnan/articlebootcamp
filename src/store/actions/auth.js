import {
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    SET_INIT,
    LOGOUT,
} from './types';

import AuthService from './../../services/auth-service';

export const setInit = (result) => {
    return {
        type: SET_INIT,
        isLoggedIn: !result[0][1],
        user: result[1][1],
    };
};

export const login = (username, password) => (dispatch) => {
    return AuthService.login(username, password).then(
        (data) => {
            dispatch({
                type: LOGIN_SUCCESS,
                payload: { user: data },
            });

            return Promise.resolve();
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            dispatch({
                type: LOGIN_FAIL,
            });

            dispatch({
                type: SET_MESSAGE,
                payload: message,
            });

            return Promise.reject();
        },
    );
}


export const logout = () => (dispatch) => {
    AuthService.logout();
    dispatch({
        type: LOGOUT,
    });
};