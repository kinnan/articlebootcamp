import {
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT,
    SET_INIT,
} from '../actions/types';

const initialState = {
    isLoggedIn: false,
    user: '',
};

export default function (state = initialState, action) {
    const { type, payload } = action;

    switch (type) {
        case SET_INIT:
            return {
                ...state,
                isLoggedIn: action.isLoggedIn,
                user: action.user,
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                isLoggedIn: true,
                user: payload.user,
            };
        case LOGIN_FAIL:
            return {
                ...state,
                isLoggedIn: false,
                user: null,
            };
        case LOGOUT:
            return {
                ...state,
                isLoggedIn: false,
                user: null,
            };
        default:
            return state;
    }
}
