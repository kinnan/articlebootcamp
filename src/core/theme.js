import { DefaultTheme } from 'react-native-paper';

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#0A5388',
    secondary: '#e75524',
    error: '#f13a59',
  },
};
