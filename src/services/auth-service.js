
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

// const API_URL = 'https://user-article.herokuapp.com/api/';
const API_URL = 'https://user-article.herokuapp.com/api/auth/';

const register = (name, username, email, password) => {
    return axios.post(API_URL + 'signup', {
        name,
        username,
        email,
        password,
    });
};

const login = (username, password) => {
    return axios
        .post(API_URL + 'signin', {
            username,
            password,
        })
        .then((response) => {
            if (response.data.accessToken) {
                AsyncStorage.setItem('user', JSON.stringify(response.data));
                console.log('data respom' + response.data);
            }

            return response.data;
        });
};

const forgotPassword = () => {
    return axios
        .get('https://user-article.herokuapp.com/api/users/password')
};
const logout = () => {
    AsyncStorage.removeItem('user');
};

export default {
    register,
    login,
    logout,
    forgotPassword
};
